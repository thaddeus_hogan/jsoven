package com.thogan.jsoven.descriptor

import org.junit.Test
import org.junit.Assert._
import com.thogan.jsoven.Resources

class TestDescriptor {

    @Test
    def testReadDescriptor() {
        val file = Resources.createConfFile()

        val descriptor = Descriptor.fromFile(file)
        file.delete()

        assertEquals("src", descriptor.src)
        assertEquals("target", descriptor.target)
        assertEquals(8080, descriptor.port)

        println(descriptor.bake)
        println(descriptor.html)
    }
}
