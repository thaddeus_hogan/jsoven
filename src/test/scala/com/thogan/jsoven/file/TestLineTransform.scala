package com.thogan.jsoven.file

import org.junit.{Ignore, Test}
import org.junit.Assert._
import java.io.{FileWriter, PrintWriter, File}
import com.thogan.jsoven.descriptor.CmdLine
import com.thogan.jsoven.Resources

class TestLineTransform {

    @Test
    def testTransformCmdLine() {
        val file = Resources.createConfFile()

        val cmds = LineTransform(file)(line => List(CmdLine.parse(line))).toList

        cmds foreach { cmd => println(cmd) }

        file.delete()
    }
}
