package com.thogan.jsoven

import java.io.{FileWriter, PrintWriter, File}

object Resources {
    def createConfFile(): File = {
        val conf =
            """src src
              |target target
              |port 8080
              |
              |bake libs
              |    script    [lib]/**/*.js           js/
              |
              |bake wspwr-app
              |    script    js/global.js             js/wspwr.js
              |    script    js/controller/**/*.js    js/wspwr.js
              |    script    js/ngapp.js              js/wspwr.js
              |    script    js/start.js              js/wspwr.js
              |    script    "js/Shitty File.js"
              |    css       css/style.css
              |
              |html [html]/**/*.html
              |    include   libs         index.html
              |    include   wspwr-app    index.html
            """.stripMargin

        val file = File.createTempFile("jsoven", "TestLineTransform")
        val writer = new PrintWriter(new FileWriter(file))
        writer.print(conf)
        writer.close()

        file
    }

    def tempDir: File = {
        val temp = File.createTempFile("test", "DirWalk")
        temp.getParentFile
    }
}
