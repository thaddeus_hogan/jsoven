package com.thogan.jsoven

import descriptor.{CmdLine, Glob}
import org.junit.{Ignore, Test}
import org.junit.Assert._
import Util._
import java.io.File

class TestUtil {

    @Test
    def testNoAccumulator() {
        assertTrue(globMatch("testFile.js", "testFile.js"))
        assertFalse(globMatch("testFile.js", "testOther.js"))
        assertFalse(globMatch("testFile.js", "testFi"))
    }

    @Test
    def testSimpleAccumulator() {
        assertTrue(globMatch("*.js", "testFile.js"))
        assertFalse(globMatch("*.js", "testFile.jsp"))
        assertTrue(globMatch("Test*.scala", "TestDescriptorUtil.scala"))
        assertFalse(globMatch("Test*.scala", "TestConnector.java"))
    }

    @Test
    def testComplexAccumulator() {
        assertTrue(globMatch("*.js", "testFile.js.js"))
        assertTrue(globMatch("t***.js", "testFile.js.js"))
        assertTrue(globMatch("Test*.Fn*.js", "TestCtrlPass.FnBox.js"))
        assertTrue(globMatch("Test*.Fn*.js", "TestCtrlPass.Fx.FnBox.js"))
    }

    @Test
    def testLongFile() {
        assertTrue(globMatch("*.js",
        "reallysuperlongfilenamethatnobodywouldactuallyusehahawhoamikiddingthesearejavaprogrammersthisisprobablyanormalrunofthemilleverydaylengthinterfacenamethateverythingunderthesunwouldimplementreallysuperlongfilenamethatnobodywouldactuallyusehahawhoamikiddingthesearejavaprogrammersthisisprobablyanormalrunofthemilleverydaylengthinterfacenamethateverythingunderthesunwouldimplement.js"))
    }

    @Test
    def testDirWalk() {
        val tempDir = Resources.tempDir

        val src = new File(tempDir, "src")
        src.mkdirs()
        val src1 = new File(src, "srcTest.js")
        src1.createNewFile()

        val main = new File(src, "main")
        main.mkdirs()
        val js = new File(main, "js")
        js.mkdirs()
        val js1 = new File(js, "testOne.js")
        js1.createNewFile()
        val js2 = new File(js, "another.js")
        js2.createNewFile()

        assertEquals(2, listFiles(tempDir, "src/main/js/*.js").length)
        assertEquals(3, listFiles(tempDir, "src/**/*.js").length)
        assertEquals(1, listFiles(tempDir, "src/srcTest.js").length)

        delTree(src)
    }

    @Test
    def testParseGlob() {
        val test1 = Glob.parse("[lib]/wookie/**_/*.js")
        assertEquals("lib/wookie/**/*.js", test1.pattern)
        assertTrue(test1.lstrip == Some("lib/"))
        assertTrue(test1.flatten)

        val test2 = Glob.parse("[double/dir]/stripping/**_/*.js")
        assertEquals("double/dir/stripping/**/*.js", test2.pattern)
        assertTrue(test2.lstrip == Some("double/dir/"))
        assertTrue(test2.flatten)

        val test3 = Glob.parse("no/strip/no/**/flatten*.js")
        assertEquals("no/strip/no/**/flatten*.js", test3.pattern)
        assertTrue(test3.lstrip == None)
        assertFalse(test3.flatten)
    }

    @Test
    def testParseCmdLine() {
        val test1 = CmdLine.parse("html [html]/**/*.html")
        println(test1)
        assertEquals("html", test1.cmd)
        assertTrue(test1.args == List("[html]/**/*.html"))
        assertFalse(test1.child)

        val test2 = CmdLine.parse("    script    js/ngapp.js              js/wspwr.js")
        println(test2)
        assertEquals("script", test2.cmd)
        assertTrue(test2.args == List("js/ngapp.js", "js/wspwr.js"))
        assertTrue(test2.child)

        val test3 = CmdLine.parse("    script    \"js/Shitty File.js\"")
        println(test3)
        assertEquals("script", test3.cmd)
        assertTrue(test3.args == List("js/Shitty File.js"))
        assertTrue(test3.child)
    }
}
