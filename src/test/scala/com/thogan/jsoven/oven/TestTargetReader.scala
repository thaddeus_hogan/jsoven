package com.thogan.jsoven.oven

import org.junit.Test
import org.junit.Assert._
import java.io.File
import com.thogan.jsoven.descriptor.Descriptor
import java.nio.charset.Charset
import com.thogan.jsoven.Util

class TestTargetReader {

    @Test
    def testReader() {
        val descFile = new File("D:/dev/git/wspwr/web/jsoven.txt")
        val base = descFile.getParentFile

        val desc = Descriptor.fromFile(descFile)

        val src = new File(base, desc.src)
        val target = new File(base, desc.target)

        Util.delTree(target)

        desc.bake foreach { m =>
            Target.list(m.children, src).toList foreach { t =>
                println("Creating target: " + t.path)
                Oven.bakeTarget(t, target)
            }
        }
    }
}
