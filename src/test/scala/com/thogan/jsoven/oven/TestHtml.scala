package com.thogan.jsoven.oven

import org.junit.Test
import org.junit.Assert._
import com.thogan.jsoven.Resources._
import com.thogan.jsoven.descriptor.Descriptor
import java.io.File

class TestHtml {

    @Test
    def testHtmls() {
        val descFile = new File("D:/dev/git/wspwr/web/jsoven.txt")
        val base = descFile.getParentFile

        val desc = Descriptor.fromFile(descFile)

        val src = new File(base, desc.src)
        val target = new File(base, desc.target)

        desc.html foreach(cmd => {
            val htmls = Html.fromCmd(src, cmd)
            println(htmls)
        })
    }
}
