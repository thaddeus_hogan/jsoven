package com.thogan.jsoven.oven

import org.junit.Test
import org.junit.Assert._
import com.thogan.jsoven.descriptor.Descriptor
import java.io.File

class TestTarget {

    @Test
    def testWsPwr() {
        val base = new File("D:/dev/git/wspwr/web/src")
        val desc = Descriptor.fromFile(new File("D:/dev/git/wspwr/web/jsoven.txt"))
        desc.bake foreach { b =>
            println(b.module + ":")
            Target.list(b.children, base) foreach { t => println(t) }
        }
    }
}
