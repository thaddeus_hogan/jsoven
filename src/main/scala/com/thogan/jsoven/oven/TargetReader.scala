package com.thogan.jsoven.oven

import collection.mutable
import java.io._
import annotation.tailrec
import scala.Some

class TargetReader(target: Target) {
    val srcQ = new mutable.Queue[File]()

    var ins: Option[InputStream] = None
    var eof = false
    var lineSep = 1

    srcQ.enqueue(target.sources: _*)

    def read(buf: Array[Byte]): Int = {
        @tailrec
        def readMore(buf: Array[Byte], off: Int, bread: Int): Int = {
            if (bread < buf.size - 2) {
                if (eof) bread
                else {
                    ins match {
                        case Some(in) => {
                            val want = buf.size - bread - 2
                            val b = in.read(buf, off, want)
                            if (b == -1) {
                                nextSource()
                                if (lineSep == 1) {
                                    buf(off) = 0xA.toByte
                                    readMore(buf, off + 1, bread + 1)
                                } else {
                                    buf(off) = 0xD.toByte
                                    buf(off + 1) = 0xA.toByte
                                    readMore(buf, off + 2, bread + 2)
                                }

                            } else readMore(buf, off + b, bread + b)
                        }
                        case None => {
                            nextSource()
                            readMore(buf, off, bread)
                        }
                    }
                }
            } else bread
        }

        if (eof) -1
        else readMore(buf, 0, 0)
    }

    private def nextSource(): Boolean = {
        ins match {
            case Some(in) => in.close()
            case None =>
        }

        if (srcQ.isEmpty) {
            eof = true
            ins = None
        } else {
            ins = Some(new BufferedInputStream(new FileInputStream(srcQ.dequeue())))
            lineSep = detectLineSep(ins.get)
        }

        !eof
    }

    private def detectLineSep(in: InputStream): Int = {
        in.mark(1024)
        def check(tries: Int): Int = {
            if (tries > 0) {
                in.read() match {
                    case 0xa => 1
                    case 0xd => 2
                    case _ => check(tries - 1)
                }
            } else 1
        }

        val ls = check(1024)
        in.reset()

        ls
    }
}