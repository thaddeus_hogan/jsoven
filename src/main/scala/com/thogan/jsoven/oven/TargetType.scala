package com.thogan.jsoven.oven

object TargetType extends Enumeration {
    type TargetType = Value
    val Script, Css, Less = Value
}
