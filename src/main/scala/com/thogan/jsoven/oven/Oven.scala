package com.thogan.jsoven.oven

import java.io._
import com.thogan.jsoven.file.LineTransform
import TargetType._
import com.thogan.jsoven.descriptor.Descriptor

object Oven {
    def bakeTarget(target: Target, base: File) {
        val reader = new TargetReader(target)

        val targetFile = new File(base, target.path)
        if (!targetFile.getParentFile.exists)
            if (!targetFile.getParentFile.mkdirs())
                throw new IOException("Unable to create directory: " + targetFile.getParentFile.getAbsolutePath)

        val writer = new FileOutputStream(targetFile, false)

        val buf = new Array[Byte](65535)
        def copyChunk(r: TargetReader, w: FileOutputStream): Int = r.read(buf) match {
            case i if i >= 0 => {
                writer.write(buf, 0, i)
                copyChunk(r, w)
            }
            case i => i
        }

        copyChunk(reader, writer)

        writer.close()
    }

    def bakeHtml(html: Html, base: File, modules: Map[String, List[Target]]) {
        val targetFile = new File(base, html.path)
        if (!targetFile.getParentFile.isDirectory)
            if (!targetFile.getParentFile.mkdirs())
                throw new IOException("Could not create parent directories for HTML file: " + targetFile.getAbsolutePath)

        val writer = new PrintWriter(new FileOutputStream(targetFile))
        val xform = LineTransform(html.src) { l =>
            if (l.trim.equals("<!-- JSOVEN -->")) {
                html.includes.flatMap(i => {
                    modules.get(i) match {
                        case Some(m) => {
                            m map { t => t.typ match {
                                case Script => "<script type=\"text/javascript\" src=\"" + t.path + "\"></script>"
                                case Css => "<link rel=\"stylesheet\" href=\"" + t.path + "\" />"
                                case _ => "<!-- JSOVEN Unknown Resource: " + t.path + " -->"
                            }}
                        }
                        case None => None
                    }
                })
            } else List(l)
        }

        xform.toStream(writer)
        writer.close()
    }

    def bake(desc: Descriptor, base: File) {
        val srcDir = new File(base, desc.src)
        val targetDir = new File(base, desc.target)

        val modules = desc.bake.foldLeft[Map[String, List[Target]]](Map())( (m, cmd) => {
            val targets = Target.list(cmd.children, srcDir)
            targets foreach { t =>
                println("Creating target: " + t.path)
                bakeTarget(t, targetDir)
            }
            m + (cmd.module -> targets)
        })

        desc.html.flatMap(h => Html.fromCmd(srcDir, h)).foreach(h => {
            println("Creating HTML: " + h.path)
            Oven.bakeHtml(h, targetDir, modules)
        })
    }
}
