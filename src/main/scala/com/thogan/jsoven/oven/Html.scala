package com.thogan.jsoven.oven

import java.io.File
import com.thogan.jsoven.descriptor.{IncludeCmd, Glob, HtmlCmd}
import com.thogan.jsoven.Util._

case class Html(path: String, src: File, includes: List[String]) extends WebResource

object Html {
    def fromCmd(srcDir: File, cmd: HtmlCmd): List[Html] = {
        val srcGlob = Glob.parse(cmd.src)

        listFiles(srcDir, srcGlob.pattern).map(srcFile => {
            val srcPath = normPath(srcDir.getAbsolutePath) + "/"
            val path = relPathFromFile(srcFile, srcPath, srcGlob)
            val includes = cmd.children flatMap { c => c match {
                case IncludeCmd(module, glob) => glob match {
                    case Some(glob) if globMatch(glob, srcFile.getName) => Some(module)
                    case Some(glob) => None
                    case None => Some(module)
                }
                case _ => None
            }}
            Html(path, srcFile, includes)
        })
    }
}