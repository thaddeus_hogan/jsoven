package com.thogan.jsoven.oven

import com.thogan.jsoven.descriptor._
import TargetType._
import java.io.{IOException, File}
import com.thogan.jsoven.Util._
import scala.Some
import com.thogan.jsoven.descriptor.ScriptCmd

case class Target(path: String, typ: TargetType, sources: List[File]) extends WebResource

object Target {

    def list(cmds: List[Cmd], src: File): List[Target] = {
        map(cmds, src).values.toList
    }

    def map(cmds: List[Cmd], src: File): Map[String, Target] = explodedList(cmds, src)
        .foldLeft(Map[String, Target]()) { (m, target) =>

        m.get(target.path) match {
            case Some(t) => m + (target.path -> Target(t.path, t.typ, t.sources ::: target.sources))
            case None => m + (target.path -> target)
        }
    }

    private def explodedList(cmds: List[Cmd], src: File): List[Target] = cmds flatMap {
        case sc: ScriptCmd => parseCmd(Glob.parse(sc.src), sc.target, Script, src)
        case cc: CssCmd => parseCmd(Glob.parse(cc.src), cc.target, Css, src)
        case er: Cmd => throw new IOException("Cannot create target from command: " + er)
    }

    private def parseCmd(glob: Glob, target: Option[String], typ: TargetType, src: File): List[Target] = {
        // FIXME - Don't normalize here, leave that to the relPathFromFile function
        // Have that function also add the trailing slash IF NOT PRESENT
        val srcPath = normPath(src.getAbsolutePath) + "/"

        target match {
            case Some(t) if isDir(t) => listFiles(src, glob.pattern) map { f =>
                    Target(t + relPathFromFile(f, srcPath, glob), typ, List(f)) }
            case Some(t) => List(Target(t, typ, listFiles(src, glob.pattern)))
            case None => listFiles(src, glob.pattern) map { f =>
                Target(relPathFromFile(f, srcPath, glob), typ, List(f)) }
        }
    }
}
