package com.thogan.jsoven

import descriptor.Descriptor
import java.io.File
import oven.Oven
import Util._


object JSOven {
    def main(args: Array[String]): Unit = {
        val descFile = List(".jsoven", "descriptor.jsoven", "jsoven.txt", "jsoven")
            .map(n => new File(n)).filter(f => f.exists()).headOption

        if (descFile == None) {
            println("Could not find a JSoven descriptor file in this directory.")
            System.exit(1)
        }

        println("Loading descriptor: " + descFile.get.getAbsolutePath)
        val desc = Descriptor.fromFile(descFile.get)

        val srcDir = new File(descFile.get.getParentFile, desc.src)
        val targetDir = new File(descFile.get.getParentFile, desc.target)

        args.foreach(a => a match {
            case "clean" => {
                println("Cleaning " + targetDir.getAbsolutePath)
                delTree(targetDir)
            }
            case "bake" => {
                println("Baking " + srcDir.getAbsolutePath)
                Oven.bake(desc, descFile.get.getParentFile)
            }
            case "http" => {
                println("Starting HTTP server on port: " + desc.port)
            }
            case s => println("Unknown command: " + s)
        })

        println("All stages complete")
    }
}
