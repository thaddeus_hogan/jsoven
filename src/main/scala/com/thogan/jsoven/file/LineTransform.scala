package com.thogan.jsoven.file

import java.io._
import annotation.tailrec
import scala.Some

class LineTransform[B](file: File)(f: (String) => List[B]) {
    val reader = new BufferedReader(new FileReader(file))

    def toList: List[B] = {
        try {
            @tailrec
            def procLine(line: String, reader: BufferedReader, out: List[B]):
                        List[B] = Option(line) match {
                case Some(line) if notBlank(line) => procLine(reader.readLine(), reader, out ::: f(line))
                case Some(line) => procLine(reader.readLine(), reader, out)
                case None => out
            }

            procLine(reader.readLine(), reader, List())
        } finally reader.close()
    }

    def toStream(out: PrintWriter) {
        try {
            def procLine(line: String, reader: BufferedReader): Boolean = Option(line) match {
                case Some(line) if notBlank(line) => {
                    f(line.stripLineEnd).foreach(l => out.println(l))
                    procLine(reader.readLine(), reader)
                }
                case Some(line) => procLine(reader.readLine(), reader)
                case None => true
            }

            procLine(reader.readLine(), reader)
        } finally reader.close()
    }

    def notBlank(line: String): Boolean = (line filter { ch => ch != ' ' && ch != '\t' }).length > 0
}

object LineTransform {
    def apply[B](file: File)(f: (String) => List[B]) = new LineTransform[B](file)(f)
}
