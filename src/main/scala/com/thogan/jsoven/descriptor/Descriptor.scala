package com.thogan.jsoven.descriptor

import java.io.{IOException, File}
import com.thogan.jsoven.file.LineTransform

abstract class Cmd(val name: String) { val children: List[Cmd] = List() }
case class SrcCmd(path: String) extends Cmd("src")
case class TargetCmd(path: String) extends Cmd("target")
case class PortCmd(port: Int) extends Cmd("port")
case class BakeCmd(module: String, override val children: List[Cmd]) extends Cmd("bake")
case class ScriptCmd(src: String, target: Option[String]) extends Cmd("script")
case class CssCmd(src: String, target: Option[String]) extends Cmd("css")
case class HtmlCmd(src: String, override val children: List[Cmd]) extends Cmd("html")
case class IncludeCmd(module: String, glob: Option[String]) extends Cmd("include")

case class Descriptor(val src: String, val target: String, val port: Int,
                      val bake: List[BakeCmd], val html: List[HtmlCmd])

object Descriptor {
    def fromCmdLines(cmdLines: List[CmdLine]) = {
        val cmdMap = mapCommands(cmdLines, Map(), None, List())

        val src = cmdMap.get("src") match {
            case Some(List(SrcCmd(src))) => src
            case _ => "src"
        }

        val target = cmdMap.get("target") match {
            case Some(List(TargetCmd(target))) => target
            case _ => "target"
        }

        val port = cmdMap.get("port") match {
            case Some(List(PortCmd(port))) => port
            case _ => 8763
        }

        val bake: List[BakeCmd] = cmdMap.getOrElse("bake", List()).asInstanceOf[List[BakeCmd]]

        val html: List[HtmlCmd] = cmdMap.getOrElse("html", List()).asInstanceOf[List[HtmlCmd]]

        Descriptor(src, target, port, bake, html)
    }

    def fromFile(file: File): Descriptor = {
        fromCmdLines(LineTransform(file)(line => List(CmdLine.parse(line))).toList)
    }

    private def cmdFromLine(line: CmdLine, children: List[Cmd]): Cmd = line match {
        case CmdLine("src", _, List(src))               => SrcCmd(src)
        case CmdLine("target", _, List(target))         => TargetCmd(target)
        case CmdLine("port", _, List(port))             => PortCmd(port.toInt)
        case CmdLine("bake", _, List(module))           => BakeCmd(module, children)
        case CmdLine("script", _, List(src, target))    => ScriptCmd(src, Some(target))
        case CmdLine("script", _, List(src))            => ScriptCmd(src, None)
        case CmdLine("css", _, List(src, target))       => CssCmd(src, Some(target))
        case CmdLine("css", _, List(src))               => CssCmd(src, None)
        case CmdLine("html", _, List(src))              => HtmlCmd(src, children)
        case CmdLine("include", _, List(module, glob))  => IncludeCmd(module, Some(glob))
        case CmdLine("include", _, List(module))        => IncludeCmd(module, None)
        case _ => throw new IOException("Unparsable command line: " + line)
    }

    private def mapCommands(lines: List[CmdLine], cmdMap: Map[String, List[Cmd]], parent: Option[CmdLine],
                            children: List[Cmd]): Map[String, List[Cmd]] = lines match {
        case line :: rest => {
            if (line.child) mapCommands(rest, cmdMap, parent, cmdFromLine(line, List()) :: children)
            else { parent match {
                case Some(cl) => mapCommands(rest,
                    cmdMap + (cl.cmd -> (cmdFromLine(cl, children.reverse) :: cmdMap.getOrElse(cl.cmd, List()))),
                    Some(line), List())
                case None => mapCommands(rest, cmdMap, Some(line), List())
            }}
        }
        case _ => parent match {
            case Some(cl) => cmdMap + (cl.cmd -> (cmdFromLine(cl, children.reverse) :: cmdMap.getOrElse(cl.cmd, List())))
            case _ => cmdMap
        }
    }
}
