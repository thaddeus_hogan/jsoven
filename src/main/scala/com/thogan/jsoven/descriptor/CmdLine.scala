package com.thogan.jsoven.descriptor

import java.io.IOException
import annotation.tailrec

case class CmdLine(cmd: String, child: Boolean, args: List[String])

object CmdLine {
    def parse(line: String): CmdLine = {
        @tailrec
        def getParts(line: List[Char], inQuote: Boolean, thisPart: List[Char],
                     parts: List[String]): List[String] = line match {
            case Nil if thisPart.length > 0 => thisPart.reverse.mkString :: parts
            case Nil => parts
            case ch :: rest if ch == '"' && !inQuote => getParts(rest, true, thisPart, parts)
            case ch :: rest if ch == '"' && inQuote => getParts(rest, false, List(), thisPart.reverse.mkString :: parts)
            case ch :: rest if ch == ' ' && inQuote => getParts(rest, inQuote, ch :: thisPart, parts)
            case ch :: rest if ch == ' ' && !inQuote && thisPart.length > 0 =>
                getParts(rest, inQuote, List(), thisPart.reverse.mkString :: parts)
            case ch :: rest if ch == ' ' => getParts(rest, inQuote, thisPart, parts)
            case ch :: rest => getParts(rest, inQuote, ch :: thisPart, parts)
            case _ => throw new IOException("Parse error parsing CmdLine")
        }

        val lineList = line.toList
        val child = lineList.head == ' '
        getParts(lineList, false, List(), List()).reverse match {
            case cmd :: args => CmdLine(cmd, child, args)
            case _ => throw new IOException("Unable to parse CmdLine: " + line)
        }
    }
}