package com.thogan.jsoven.descriptor

import annotation.tailrec
import java.io.IOException

case class Glob(pattern: String, lstrip: Option[String], flatten: Boolean)

object Glob {
    /**
     * <p>Parses an extended format glob and returns a Glob object representing the pattern expressed.</p>
     *
     * <p>Supports the following:</p>
     *
     * <ul>
     *     <li>[brackets] around path elements that should be stripped when writing files for which
     *     this glob will identify sources. Ex: [lib]/&#42;&#42;/&#42;.js will indicate that lib/ should be
     *     stripped from the path if the identified files are copied somewhere.</li>
     *     <li>&#42;&#42;_ element indicates that the results of the recursive directory search should be
     *     flattened. Ex: lib/&#42;&#42;_/&#42;.js will indicate that any directories under lib should be removed
     *     from the path of each file found.</li>
     * </ul>
     *
     * <p><strong>WARNING: </strong>Flattening is un-implemented. The flattening of a directory structure
     * is error-prone beause of possible name conflicts. While &#42;&#42;_ is syntactically allowed,
     * most of JSoven's functionality will ignore the flatten flag.</p>
     *
     * <p>Whether or not to keep and fully implement or discard flattening will be re-visited later.</p>
     * <p>TODO - Flattening, keep or throw?</p>
     *
     * @param glob Extended glob pattern String
     * @return Glob object
     */
    def parse(glob: String): Glob = {
        @tailrec
        def getParts(glob: List[Char], stripping: Boolean, stripEnding: Boolean, thisPart: List[Char],
                     parts: List[(String, Boolean)]): List[(String, Boolean)] = glob match {
            case Nil => (thisPart.reverse.mkString, stripping) :: parts
            case ch :: rest if ch == '[' => getParts(rest, true, false, thisPart, parts)
            case ch :: rest if ch == ']' => getParts(rest, false, true, thisPart, parts)
            case ch :: rest if ch == '/' => getParts(rest, stripping, false, List(),
                (thisPart.reverse.mkString, stripping || stripEnding) :: parts)
            case ch :: rest => getParts(rest, stripping, stripEnding, ch :: thisPart, parts)
            case _ => throw new IOException("Failed to parse glob: " + glob.mkString)
        }

        val parts = getParts(glob.toList, false, false, List(), List()).reverse

        def doFlatten(parts: List[(String, Boolean)]): Boolean = {
            (parts flatMap { p => p._1 match {
                case "**_" => Some(true)
                case _ => None
            }}).length > 0
        }

        val pattern = parts.map(p => if (p._1 == "**_") "**" else p._1)
                .reduceLeft[String]((st, p) => st + "/" + p)
        val lstrip = parts.foldLeft("")((st, p) => if (p._2) st + p._1 + "/" else st)

        Glob(pattern, if (lstrip.length > 0) Some(lstrip) else None, doFlatten(parts))
    }
}
