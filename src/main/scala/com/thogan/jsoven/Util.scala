package com.thogan.jsoven

import descriptor.Glob
import java.io.{IOException, File}
import annotation.tailrec

object Util {
    /**
     * <p>Return a list of all files contained in <em>dir</em> that glob match <em>pattern</em></p>
     *
     * <p><strong>If pattern is not a glob</strong>, but rather just a path, a File object represening
     * that path relative to dir will be returned in a List.</p>
     *
     * <p>If the pattern contains "**", all directories encountered beyond the "**" will be recursively
     * searched for files matching the pattern.</p>
     *
     * <p>Pattern Examples:</p>
     * <ul>
     *     <li>src/main/js/&#42;&#42;/&#42;.js - search src/main/js and all directories within src/main/js
     *                               for files matching &#42;.js</li>
     *
     *     <li>src/main/css/&#42;.less - search src/main/css for files matchin *.less</li>
     * </ul>
     *
     * @param dir directory to search
     * @param pattern glob pattern used to match file names
     * @return A List of File objects representing the matched files
     */
    def listFiles(dir: File, pattern: String): List[File] = pattern match {
        case p if (isPattern(p)) => walkPat(dir, pattern.split("/").toList, false)
        case _ => List(new File(dir, pattern))
    }

    /**
     * <p>Traverse a glob pattern path and return all files matched by the pattern.</p>
     */
    private def walkPat(dir: File, pats: List[String], recurse: Boolean): List[File] = pats match {
        case fpat :: Nil => {
            dir.listFiles().toList flatMap { f =>
                if (f.isFile && globMatch(fpat, f.getName)) List(f)
                else walkPat(f, pats, true)
            }
        }
        case "**" :: fpat => walkPat(dir, fpat, true)
        case dpat :: rest => {
            val sub = new File(dir, dpat)
            if (sub.isDirectory) walkPat(sub, rest, false) else List()
        }
        case _ => throw new IOException("Unparsable path pattern: " + pats)
    }

    /**
     * <p>Match a file name against a glob pattern.</p>
     *
     * <p><strong>NOTE:</strong> The "glob" referred to in this function is the file part of
     * a full glob <em>ONLY</em>. This function does not evaluate paths, strips, or flattenings.</p>
     *
     * @param glob Glob Pattern (file part)
     * @param name File Name
     * @return Boolean true indicating a match, otherwise false
     */
    def globMatch(glob: String, name: String): Boolean = {
        def chM(name: List[Char], glob: List[Char]): Boolean = name match {
            case Nil => glob.isEmpty
            case n :: ns => glob match {
                case g :: gs =>
                    (n == g && chM(ns, gs)) || // current chars match and rest of list matches OR
                    (g == '*' &&               // current glob char is * AND
                        (chM(ns, gs)) ||       // rest of lists match OR
                        (ns.length > gs.length && chM(ns, glob))) // if list ends not aligned, shift name left and test again
                case Nil => false
            }
        }

        chM(name.toList, glob.toList)
    }

    /**
     * <p>Delete the specified directory and all files and directories within.</p>
     * <p>If this function throws an Exception, the directory contents will be left partially deleted.</p>
     *
     * @param dir File object representing the directory to delete
     */
    def delTree(dir: File): Unit =  if (dir.exists()) dir.listFiles foreach { f =>
        if (f.isDirectory) delTree(f)
        try { f.delete() } catch { case e: Exception => println("Did not delete " + f.getAbsolutePath) }
    }

    /**
     * <p>Returns true if the provided string is a glob pattern, false if it refers to
     * a single file.</p>
     */
    def isPattern(pattern: String): Boolean = pattern.contains("*")

    /**
     * <p>Returns a path String that has any back-slash separators converted into
     * forward slashes.</p>
     */
    def normPath(path: String): String = {
        def chM(path: List[Char], out: List[Char]): List[Char] = path match {
            case ch :: rest if ch == '\\' => chM(rest, '/' :: out)
            case ch :: rest => chM(rest, ch :: out)
            case _ => out.reverse
        }
        chM(path.toList, List()).mkString
    }

    /**
     * <p>Returns the relative path of the specified file. Relative to srcPath with any
     * path stripping determined by the glob applied.</p>
     *
     * @param file File for which to acquire the relative path
     * @param srcPath Path that the returned path should be relative to
     * @param glob Glob that may include path stripping information
     * @return A relative path to "file" from "srcPath" with any stripping from "glob" applied
     */
    def relPathFromFile(file: File, srcPath: String, glob: Glob): String = glob.lstrip match {
        case Some(strip) => stripPath(normPath(file.getAbsolutePath), srcPath + strip)
        case None => stripPath(normPath(file.getAbsolutePath), srcPath)
    }

    /**
     * <p>Returns whether or not the provieded path String is a directory.</p>
     *
     * @param path Path to evaluate
     * @return True if directory, false if file
     */
    def isDir(path: String): Boolean = path.endsWith("/")

    def stripPath(path: String, strip: String): String = {
        def chM(path: List[Char], strip: List[Char]): String = strip match {
            case s :: ns if s != path.head => path.mkString
            case s :: ns => chM(path.tail, ns)
            case _ => path.mkString
        }

        if (strip.length > path.length) throw new IOException("Strip longer than path! " + strip + " - " + path)
        if (path.length > 0 && strip.length > 0) chM(path.toList, strip.toList)
        else path
    }
}